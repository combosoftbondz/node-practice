
const Sequelize = require('sequelize');

const sequelize = require('../util/database');

const Product = sequelize.define('products',{
    id: {
        type: Sequelize.INTEGER,
        autoIncrement: true,
        allowedNull: false,
        primaryKey: true
    },
    title: Sequelize.STRING,
    price: {
        type: Sequelize.DOUBLE,
        allowedNull: false
    },
    imageUrl: {
        type: Sequelize.STRING,
        allowedNull: false
    },
    description: {
        type: Sequelize.STRING,
        allowedNull: false
    }
});
module.exports = Product;














// //Beneth code is for sql and file system data manupulation

// // const fs = require('fs');
// // const path = require('path');
// const db = require('../util/database');
// const Cart = require('./cart');
// const { DOUBLE } = require('sequelize');
// // const products = [];

// //path of the file
// // const p = path.join(
// //     path.dirname(process.mainModule.filename),
// //     'data',
// //     'products.json'
// //     );

// //get all product from file
// // const getProductsFromFile = (cb) => {
    
// //     fs.readFile(p, (err, fileContent) => {
// //         if(err){
// //             cb([]);
// //         }else{
// //             cb(JSON.parse(fileContent));
// //         }
// //     });
// // };
// module.exports = class Product{
//     constructor(id, title, imageUrl, description, price){
//         this.id = id;
//         this.title = title;
//         this.imageUrl = imageUrl;
//         this.description = description;
//         this.price = price;
//     }

//     save(){

//         return db.execute('INSERT INTO products (title, price, description, imageUrl) VALUES (?, ?, ?, ?)', [this.title, this.price, this.description, this.imageUrl]);

//         // products.push(this);
        

//         //for save in file
//         // getProductsFromFile((products) => {
//         //     if(this.id){
//         //         const getProdcutIndex = products.findIndex(prod => prod.id === this.id);
//         //         const updatedProduct = [...products];
//         //         updatedProduct[getProdcutIndex] = this;
//         //         fs.writeFile(p, JSON.stringify(updatedProduct),(err) => {
//         //             console.log(err);
//         //         });
//         //     }else{
//         //         this.id = Math.random().toString();
//         //         products.push(this);
//         //         fs.writeFile(p, JSON.stringify(products),(err) => {
//         //             console.log(err);
//         //         });
//         //     }
//         // });
//     }

//     static fetchAll(){

//         return db.execute('SELECT * FROM products');

//         //for fetch product from file
//         // getProductsFromFile(cb);
//         // return products;
//     }

//     static findById(id){

//         return db.execute('SELECT * FROM products WHERE products.id = ?',[id]);
//         //for find from file
//         // getProductsFromFile(products => {
//         //     const product = products.find(p => p.id === id);
//         //     cb(product);
//         // })
//     }

//     static deleteById(id){
//         getProductsFromFile(products => {
//             // const productIndex = products.findIndex(p => p.id === id);
//             // const updateArray = products.splice(productIndex, 1);
//             // fs.writeFile(p, JSON.stringify(products),(err) => {
//             //     console.log(err);
//             // });
//             // cb([]);

//             //for delete from file
//             // const prodcuct = products.find(prod => prod.id === id);
//             // const updatedProducts = products.filter(prod => prod.id !== id);
//             // fs.writeFile(p, JSON.stringify(updatedProducts),(err) => {
//             //     if(!err){
//             //         Cart.deleteProduct(id,prodcuct.price);
//             //     }
//             // });
            
//         })
//     }
// }