const Product = require('../models/product');
const Cart = require('../models/cart');
// const products = [];

  exports.getIndex = (req, res, next) => {

    Product.findAll().then((products) => {
      res.render('shop/index',{
          prods: products, 
          pageTitle: 'Shop', 
          path: '/'
      });
    }).catch(err => console.log(err));


    //for sql fetch all
  //   Product.fetchAll()
  //   .then(([rows, fieldData]) => {
  //     res.render('shop/index',{
  //       prods: rows, 
  //       pageTitle: 'Shop', 
  //       path: '/'
  //   });
  // })
  //   .catch(err => console.log(err));

  //   Product.fetchAll((products) => {
  //     res.render('shop/index',{
  //     prods: products, 
  //     pageTitle: 'Shop', 
  //     path: '/', 
  //     hasProducts: products.length > 0,
  //     activeShop: true,
  //     productCSS: true});
  // });
  };

  exports.getProducts = (req, res, next) => {
    //   console.log(adminData.products);
    // res.sendFile(path.join(rootDir, 'views', 'shop.html'));
    // const products = adminData.products;


    Product.findAll().then((products) => {
        res.render('shop/product-list',{
        prods: products, 
        pageTitle: 'Products', 
        path: '/products', 
        });
    }).catch(err => console.log(err));

    //for sql all products
    // Product.fetchAll()
    // .then(([rows, fieldData]) => {
    //   res.render('shop/product-list',{
    //     prods: rows, 
    //     pageTitle: 'Products', 
    //     path: '/products', 
    //     });
    // })
    // .catch(err => console.log(err));

  //   Product.fetchAll((products) => {
  //     res.render('shop/product-list',{
  //     prods: products, 
  //     pageTitle: 'Products', 
  //     path: '/products', 
  //     });
  // });
  };

  exports.getProduct = (req, res, next) => {
    const prodId = req.params.productId;

    Product.findByPk(prodId).then(product => {
      res.render('shop/product-detail',{
        product: product,
        path: '/products',
        pageTitle: product.title
      });
    }).catch(err => console.log(err));

    // Product.findAll({where : {id: prodId}})
    // .then(product => {
    //   res.render('shop/product-detail',{
    //     product: product[0],
    //     path: '/products',
    //     pageTitle: product[0].title
    //   });
    // }).catch(err => console.log(err));


    //for sql product detail
    // Product.findById(prodId).then(([product]) => {
    //   res.render('shop/product-detail',{
    //     product: product[0],
    //     path: '/products',
    //     pageTitle: product.title
    //   });
    // }).catch(err => console.log(err));

    //callback with file data
    // Product.findById(prodId, product => {
    //   res.render('shop/product-detail',{
    //     product: product,
    //     path: '/products',
    //     pageTitle: product.title
    //   });
    // });
    // console.log(prodId);
    // res.redirect('/');
  };

  exports.getCart = (req, res, next) => {
    Cart.getCart(cart => {
      Product.fetchAll(products => {
        let cartProducts = [];
        for(product of products){
          const cartProductData = cart.products.find(prod => prod.id === product.id);
          if(cartProductData){
            cartProducts.push({productData: product, qnty: cartProductData.qnty});
          }
        }
        console.log(cartProducts);
        res.render('shop/cart',{
          path: '/cart',
          pageTitle: 'Your Cart',
          products: cartProducts
        });
      });
    });
  };

  exports.postCart = (req, res, next) => {
    const prodId = req.body.productId;
    Product.findById(prodId, (product) => {
      Cart.addCart(prodId, product.price);
    });
    // console.log(prodId);
    res.redirect('/cart');
  };

  exports.deleteCartItem = (req, res, next) => {
    const prodId = req.body.productId;
    Product.findById(prodId, product => {
      Cart.deleteProduct(prodId, product.price);
      res.redirect('/cart');
    });
  };

  exports.getOrders = (req, res, next) => {
    res.render('shop/orders',{
      path: '/orders',
      pageTitle: 'Yours Orders'
    });
  };

  exports.getCheckout = (req, res, next) => {
    res.render('shop/checkout',{
      path: '/checkout',
      pageTitle: 'Checkout'
    });
  };
