const Product = require('../models/product');
exports.getAddProduct = (req, res, next) => {
    // res.sendFile(path.join(rootDir, 'views', 'add-product.html'));
    res.render('admin/edit-product',{
      pageTitle: 'Add Product', 
      path: '/admin/add-product',
      editing: false
    });
  };

  exports.postAddProduct = (req, res, next) => {
    // products.push({title: req.body.title});
    const title = req.body.title;
    const imageUrl = req.body.imageUrl;
    const description = req.body.description;
    const price = req.body.price;
    const product = new Product(null, title, imageUrl, description, price);

    Product.create({
      title: title,
      price: price,
      imageUrl: imageUrl,
      description: description
    }).then((result) => {
      console.log('Product Created');
      res.redirect('/admin/products');
    }).catch(err => console.log(err));

    //for sql query
    // product.save().then((result) => {
    //   res.redirect('/');
    // }).catch(err => console.log(err));
  };

  exports.getEditProduct = (req, res, next) => {
    const editMode = req.query.edit;
    if(!editMode){
      return res.redirect('/');
    }
    const prodId = req.params.productId;
    Product.findByPk(prodId).then(product => {
      res.render('admin/edit-product',{
        pageTitle: 'Edit Product', 
        path: '/admin/edit-product',
        editing: editMode,
        product: product
      });
    }).catch(err => console.log(err));
  };

  exports.postEditProduct = (req, res, next) => {
    const productId = req.body.productId;
    const updatedTitle = req.body.title;
    const updatedImageUrl = req.body.imageUrl;
    const updatedPrice = req.body.price;
    const updatedDescription = req.body.description;

    Product.findByPk(productId)
    .then(product => {
      product.title = updatedTitle;
      product.price = updatedPrice;
      product.description = updatedDescription;
      product.imageUrl = updatedImageUrl;
      return product.save();
    })
    .then(result => {
      console.log('Updated Product');
      res.redirect('/admin/products');
    })
    .catch(err => console.log(err));

    //for sql query
    // const updatedProduct = new Product(productId, updatedTitle, updatedImageUrl, updatedDescription, updatedPrice);
    // updatedProduct.save();
    // res.redirect('/admin/products');
  };

  exports.postDeleteProduct = (req, res, next) => {
    const prodId = req.body.productId;
    Product.findByPk(prodId)
    .then(product => {
      return product.destroy();
    })
    .then(result => {
      console.log('Deleted Successfully');
      res.redirect('/admin/products');
    })
    .catch(err => console.log(err));


    //for sql or file 
    // Product.deleteById(prodId);
    // res.redirect('/admin/products');
  };

  exports.getProducts = (req, res, next) => {

    Product.findAll()
    .then(products => {
      res.render('admin/products',{
        prods: products, 
        pageTitle: 'Admin Products', 
        path: '/admin/products'
        });
    }).catch(err => console.log(err));

    // Product.fetchAll((products) => {
    //     res.render('admin/products',{
    //     prods: products, 
    //     pageTitle: 'Admin Products', 
    //     path: '/admin/products'
    //     });
    // });
  };